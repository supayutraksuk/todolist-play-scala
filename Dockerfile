FROM hseeberger/scala-sbt:8u181_2.12.6_1.2.1

# Creating working directory
RUN mkdir /todolist
WORKDIR /todolist

# Copying source
COPY . .
