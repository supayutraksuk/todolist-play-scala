# Task schema

# --- !Ups
create table `task` (
  `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `title` TEXT NOT NULL,
  `detail` TEXT NOT NULL
)

# --- !Downs
drop table `task`