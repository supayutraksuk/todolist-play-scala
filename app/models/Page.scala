package models

import play.api.libs.json._
import play.api.libs.functional.syntax._


case class Page(rows: Seq[Task], page: Int, limit: Long, total: Long)

object Page {
  implicit val pageWrite: Writes[Page] = (
    (JsPath \ "rows").write[Seq[Task]] and
      (JsPath \ "page").write[Int] and
      (JsPath \ "limit").write[Long] and
      (JsPath \ "total").write[Long]
    )(unlift(Page.unapply))
}