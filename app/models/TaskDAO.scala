package models

import javax.inject.Inject
import play.api.db.slick.{ DatabaseConfigProvider, HasDatabaseConfigProvider }
import scala.concurrent.Future
import slick.jdbc.JdbcProfile
import slick.jdbc.MySQLProfile.api._
import scala.concurrent.ExecutionContext.Implicits.global

/*
 * Task Data Access Object
**/
class TaskDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[JdbcProfile] {

  val tasks = TableQuery[TaskTable]

  def view(id: Long): Future[Option[Task]] = {
    db.run(tasks.filter(_.id === id).result.headOption)
  }

  def count(): Future[Int] = {
    db.run(tasks.map(_.id).length.result)
  }

  def list(page: Int = 1, limit: Int = 10): Future[Page] = {
    val offset = limit * (page-1)

    for {
      totalRows <- count()
      result <- db.run(tasks.drop(offset).take(limit).result)
    } yield Page(result, page, limit, totalRows)
  }

  def insert(task: Task): Future[Unit] = db.run(tasks += task).map(_ => Some("Success"))

  def update(id: Long, task: Task): Future[Boolean] = {
    val newTask: Task = task.copy(Some(id))
    db.run(tasks.filter(_.id === id).update(newTask)).map { affectedRows =>
      affectedRows > 0
    }
  }

  def delete(id: Long): Future[Boolean] = {
    db.run(tasks.filter(_.id === id).delete).map { affectedRows =>
      affectedRows > 0
    }
  }
}