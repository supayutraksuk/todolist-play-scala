package models

import slick.jdbc.MySQLProfile.api._
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

case class Task(id: Option[Long], title: String, detail: String)


/*
 * Task Table Definition
**/
class TaskTable(tag: Tag) extends Table[Task](tag, "task") {
  def id = column[Option[Long]]("id", O.PrimaryKey,O.AutoInc)
  def title = column[String]("title")
  def detail = column[String]("detail")

  def * = (id, title, detail) <> ((Task.apply _).tupled, Task.unapply)
}

object Task {
  implicit val taskRead: Reads[Task] = (
    (JsPath \ "id").readNullable[Long] and
      (JsPath \ "title").read[String](minLength[String](3)) and
      (JsPath \ "detail").read[String]
    )(Task.apply _)

  implicit val taskWrite: Writes[Task] = (
    (JsPath \ "id").write[Option[Long]] and
      (JsPath \ "title").write[String] and
      (JsPath \ "detail").write[String]
    )(unlift(Task.unapply))
}
