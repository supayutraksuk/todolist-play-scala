package controllers

import javax.inject.{Inject, Singleton}
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}

@Singleton
class APIDocController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def index() = Action { implicit request: Request[AnyContent] =>
    val base_url: String = routes.APIDocController.index().absoluteURL().replaceAll("docs","")
    Redirect("/swagger/?url="+base_url+"swagger.json")
  }
}
