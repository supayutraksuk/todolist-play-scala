package controllers.api


import play.api.libs.json._
import play.api.mvc._
import javax.inject.Inject
import scala.concurrent.Future
import models.{Task, TaskDAO}
import scala.concurrent.ExecutionContext.Implicits.global
//import io.swagger.util.Json
import io.swagger.annotations._

@Api(value = "/tasks", description = "Operations about tasks")
class TodolistApiController @Inject() (taskDao: TaskDAO, cc: ControllerComponents) extends AbstractController(cc) {

  @ApiOperation(
    value = "All tasks",
    response = classOf[models.Task],
  )
  @ApiResponses(Array(
    new ApiResponse(code = 400, message = "Invalid ID"),
    new ApiResponse(code = 404, message = "Task not found")))
  def index(
             @ApiParam(value = "page") page: Int,
             @ApiParam(value = "limit") limit:Int) = Action.async { request =>
    taskDao.list(page = page, limit = limit).map { data =>
      Ok(Json.toJson(data)).as("application/json")
    }
  }

  @ApiOperation(
    value = "Create tasks",
    code = 201
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(value = "Task's title", required = true, dataType = "models.Task", paramType = "body")
  ))
  def create = Action.async(parse.json) { request =>
    request.body.validate[Task].map { task =>
      taskDao.insert(task).map {
        result => Created(Json.obj("status" -> "success")).as("application/json")
      }.recoverWith {
        case e => Future { InternalServerError("ERROR: " + e )}
      }
    }.recoverTotal {
      e => Future { BadRequest( Json.obj("status" -> "fail", "data" -> JsError.toJson(e)) ) }
    }
  }

  @ApiOperation(
    value = "Find task by ID",
    response = classOf[models.Task],
  )
  def read(
            @ApiParam(value = "ID of the task") id: Long) = Action.async { request =>
    taskDao.view(id).map { data =>
      data match {
        case Some(task) => Ok(Json.toJson(task)).as("application/json")
        case None => UnprocessableEntity
      }
    }
  }

  @ApiImplicitParams(Array(
    new ApiImplicitParam(value = "Task's title", required = true, dataType = "models.Task", paramType = "body")
  ))
  def update(id: Long) = Action.async(parse.json) { request =>
    request.body.validate[Task].map { task =>
      taskDao.update(id, task).map { result =>
        result match {
          case true => Ok(Json.obj("status" -> "success")).as("application/json")
          case false => UnprocessableEntity
        }
      }.recoverWith {
        case e => Future { InternalServerError("ERROR: " + e ) }
      }
    }.recoverTotal {
      e => Future { BadRequest( Json.obj("status" -> "fail", "data" -> JsError.toJson(e)) ) }
    }
  }

  def delete(id: Long) = Action.async { request =>
    taskDao.delete(id).map { result =>
      result match {
        case true => Ok(Json.obj("status" -> "success")).as("application/json")
        case false => UnprocessableEntity
      }
    }.recoverWith {
      case e => Future { InternalServerError("ERROR: " + e ) }
    }
  }
}