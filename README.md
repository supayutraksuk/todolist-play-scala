# Todolist Rest API with Playframework

![Scheme](https://bitbucket.org/supayutraksuk/todolist-play-scala/raw/master/docs.png)

### Requirement

- Scala 2.12.6
- Play framework 2.6.19
- MySQL 5.7.24

API Document: [http://localhost:9000/docs](http://localhost:9000/docs)

#### Configuration

in application.conf

run with local environtment use `slick.dbs.default.db.url = "jdbc:mysql://localhost/todolist"`

run with docker-compose use `slick.dbs.default.db.url = "jdbc:mysql://mysql/todolist"`


#### How to run (normal)

```
$ sbt run
```

#### How to run with docker-compose (Please stop mysql on your local before run this command)

```
$ docker-compose up
```

**For now run with docer-compose are still have bugs and I already open [issues](https://bitbucket.org/supayutraksuk/todolist-play-scala/issues/1/unexpected-exception-when-use-docker)**

```
I try to setup docker-compose because I want to use this project on any OS/Machine
for development environment. we don't need to install scala, sbt, mysql and prevent bugs from conflict version.
    
```
